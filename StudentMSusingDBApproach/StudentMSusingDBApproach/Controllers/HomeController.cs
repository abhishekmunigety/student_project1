﻿using StudentMSusingDBApproach.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentMSusingDBApproach.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home

        StudentDBEntities1 db = new StudentDBEntities1();
        public ActionResult Index()
        {
            var data = db.StudentsDBEFs.ToList();
            return View(data);
        }

        public ActionResult Create()
        {
          
            return View();
        }
        [HttpPost]
        public ActionResult Create(StudentsDBEF s)
        {
            if (ModelState.IsValid == true)
            {
                db.StudentsDBEFs.Add(s);
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["Insertedmessage"] = "Message Inserted !!";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Insertedmessage"] = "<script>alert('Message Not Inserted')</script>";
                }
            }

                return View();
            
        }

        public ActionResult Edit(int id)
        {
            var row = db.StudentsDBEFs.Where(Model => Model.StudentId == id).FirstOrDefault();
            return View(row);
        }

        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult Edit(StudentsDBEF s)
        {
           if(ModelState.IsValid == true)
            {
                db.Entry(s).State = EntityState.Modified;
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["Updatedmessage"] = " Message Updated ";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["UpdatedMessage"] = "<script>alert('Message Not Updated')</script>";
                }
            }
            return View();
        }


        public ActionResult Delete(int id)
        {

            if (id > 0)
            {


                var Deletedrow = db.StudentsDBEFs.Where(Model => Model.StudentId == id).FirstOrDefault();

                if(Deletedrow!=null)
                {
                    db.Entry(Deletedrow).State = EntityState.Deleted;
                    int a = db.SaveChanges();
                    if (a > 0)
                    {
                        TempData["Deletedmessage"] = "Message Deleted";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["DeletedMessage"] = "<script>alert('Message Not Deleted')</script>";
                    }
                }
            }
            
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            var row = db.StudentsDBEFs.Where(Model => Model.StudentId == id).FirstOrDefault();
            return View(row);
        }

        public ActionResult GenderList()
        {
            var list = new List<string>() { "male", "female", "other" };
            ViewBag.list=list;
            return View();
        }

        public ActionResult MaritialList()
        {
            var MS = new List<string>() { "single", "married"};
            ViewBag.MS = MS;
            return View();
        }
    }
}