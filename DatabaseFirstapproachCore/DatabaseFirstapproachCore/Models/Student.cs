﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace DatabaseFirstapproachCore.Models
{
    public partial class Student
    {
        [Required(ErrorMessage ="ID Required")]

    [StringLength(10,MinimumLength =1)]
        public string StudentId { get; set; }

        [Required(ErrorMessage ="FirstName is Required")]
        [StringLength(15, MinimumLength = 3)]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "LastName is Required")]
        [StringLength(15, MinimumLength = 3)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is Required")]
        [StringLength(20, MinimumLength = 5)]

        [EmailAddress]
        [RegularExpression("^[a - zA - Z0 - 9 + _.-] +@[a-zA-Z0-9.-]+$")]
        public string Email { get; set; }

        [Required(ErrorMessage = "PhoneNumber is Required")]
        [StringLength(10, MinimumLength = 10,ErrorMessage ="the length should be between 3 and 15")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "DateofBirth is Required")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Fill this is Required")]
        public string MaritialStatus { get; set; }

        [Required(ErrorMessage = "Gender is Required")]
        public string Gender { get; set; }
    }
}
